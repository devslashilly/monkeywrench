module info.mdhs.monkeywrench.api {
    exports info.mdhs.monkeywrench.api;
    exports info.mdhs.monkeywrench.api.annotation.access;
    exports info.mdhs.monkeywrench.api.inject;
    exports info.mdhs.monkeywrench.api.wrench;
}