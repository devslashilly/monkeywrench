package info.mdhs.monkeywrench.test;

import info.mdhs.monkeywrench.api.IMonkeyWrench;
import info.mdhs.monkeywrench.api.annotation.Bind;
import info.mdhs.monkeywrench.api.wrench.IWrenchRegistry;


public class TestWrench implements IMonkeyWrench
{
    @Bind
    IWrenchRegistry wrenchRegistry;

    @Override
    public IWrenchRegistry getWrenchRegistry()
    {
        return this.wrenchRegistry;
    }
}
