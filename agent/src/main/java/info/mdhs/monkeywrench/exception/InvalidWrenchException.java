package info.mdhs.monkeywrench.exception;

public class InvalidWrenchException extends Exception
{
    public InvalidWrenchException(String message)
    {
        super(message);
    }
}
