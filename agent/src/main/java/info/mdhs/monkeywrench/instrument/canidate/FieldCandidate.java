/*
 * Copyright (c) 2019. Anthony Anderson
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

package info.mdhs.monkeywrench.instrument.canidate;

import info.mdhs.monkeywrench.api.annotation.access.AccessTransform.Access;

import java.lang.reflect.Type;

public class FieldCandidate
{

    String name;

    Type type;

    boolean isStatic;

    boolean isDinal;

    Access access;

    public FieldCandidate(String name, Type type, boolean isStatic, boolean isDinal, Access access)
    {
        this.name = name;
        this.type = type;
        this.isStatic = isStatic;
        this.isDinal = isDinal;
        this.access = access;
    }

    public String getName()
    {
        return name;
    }

    public Type getType()
    {
        return type;
    }

    public boolean isStatic()
    {
        return isStatic;
    }

    public boolean isDinal()
    {
        return isDinal;
    }

    public Access getAccess()
    {
        return access;
    }
}
