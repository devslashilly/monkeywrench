/*
 * Copyright (c) 2019. Anthony Anderson
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

package info.mdhs.monkeywrench.instrument;

import info.mdhs.monkeywrench.wrench.IWrench;

import java.util.Set;

public class InstrumentationHandler
{
    private Set<IWrench> wrenches;

    public InstrumentationHandler(Set<IWrench> wrenches)
    {
        this.wrenches = wrenches;
    }
}
