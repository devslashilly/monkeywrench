/*
 * Copyright (c) 2019. Anthony Anderson
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

package info.mdhs.monkeywrench.instrument.canidate;

import info.mdhs.monkeywrench.api.annotation.access.AccessTransform.Access;

import java.lang.reflect.Type;

public class MethodCandidate
{
    String name;

    Type returnType;

    Access access;

    public MethodCandidate(String name, Type returnType, Access access)
    {
        this.name = name;
        this.returnType = returnType;
        this.access = access;
    }

    public String getName()
    {
        return name;
    }

    public Type getReturnType()
    {
        return returnType;
    }

    public Access getAccess()
    {
        return access;
    }
}
