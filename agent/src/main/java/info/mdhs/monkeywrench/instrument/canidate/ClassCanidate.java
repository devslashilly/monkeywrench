/*
 * Copyright (c) 2019. Anthony Anderson
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 */

package info.mdhs.monkeywrench.instrument.canidate;

import info.mdhs.monkeywrench.api.annotation.access.AccessTransform.Access;

import java.util.Set;

public class ClassCanidate
{
    String name;

    String packageName;

    String cononicalName;

    Set<FieldCandidate> feildCanidtates;

    Set<MethodCandidate> methodCanidates;

    Access access;

    public ClassCanidate(String name, String packageName, String cononicalName, Set<FieldCandidate> feildCanidtates, Set<MethodCandidate> methodCanidates, Access access)
    {
        this.name = name;
        this.packageName = packageName;
        this.cononicalName = cononicalName;
        this.feildCanidtates = feildCanidtates;
        this.methodCanidates = methodCanidates;
        this.access = access;
    }

    public String getName()
    {
        return name;
    }

    public String getPackageName()
    {
        return packageName;
    }

    public String getCononicalName()
    {
        return cononicalName;
    }

    public Set<FieldCandidate> getFeildCanidtates()
    {
        return feildCanidtates;
    }

    public Set<MethodCandidate> getMethodCanidates()
    {
        return methodCanidates;
    }

    public Access getAccess()
    {
        return access;
    }
}
