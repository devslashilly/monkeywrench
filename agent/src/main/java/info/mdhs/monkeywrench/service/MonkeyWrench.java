package info.mdhs.monkeywrench.service;

import info.mdhs.monkeywrench.api.IMonkeyWrench;
import info.mdhs.monkeywrench.api.annotation.Bind;
import info.mdhs.monkeywrench.api.wrench.IWrenchRegistry;
import info.mdhs.monkeywrench.instrument.InstrumentationHandler;
import info.mdhs.monkeywrench.wrench.IWrench;
import info.mdhs.monkeywrench.wrench.Wrench;
import info.mdhs.monkeywrench.wrench.WrenchRegistry;

import java.util.Arrays;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

public class MonkeyWrench implements IMonkeyWrench
{
    private ServiceLoader<IMonkeyWrench> wrenchServiceLoader;
    private IWrenchRegistry wrenchRegistry;
    private Set<IWrench> wrenches;
    private InstrumentationHandler ihandler;


    public MonkeyWrench()
    {
        wrenchServiceLoader = ServiceLoader.load(IMonkeyWrench.class);
        wrenches = new HashSet<>();
        wrenchRegistry = new WrenchRegistry();
        wrenchServiceLoader.forEach(iMonkeyWrench ->
        {
            var clazz = iMonkeyWrench.getClass();
            Arrays.asList(clazz.getDeclaredFields()).forEach(field->
            {
                if (field.isAnnotationPresent(Bind.class))
                {
                    if (field.getType() == IWrenchRegistry.class)
                    {
                        try
                        {
                            field.set(null, wrenchRegistry);
                        } catch (IllegalAccessException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            });
        });

    }

    @Override
    public IWrenchRegistry getWrenchRegistry()
    {
        return this.wrenchRegistry;
    }

    public void initalizeWrenches()
    {
        this.getWrenchRegistry().getWrenchRegistry().forEach(i->
        {
            if (!i.getClass().isAnnotationPresent(info.mdhs.monkeywrench.api.annotation.MonkeyWrench.class))
            {
                System.out.print(i.getClass().getCanonicalName() +" is not a wrench ignoring");
            }
            else
            {
                var wrench = i.getClass();
                var target = i.getClass().getAnnotation(info.mdhs.monkeywrench.api.annotation.MonkeyWrench.class).value();
                wrenches.add(new Wrench(wrench, target));
            }
        });
    }

    public Set<IWrench> getWrenches()
    {
        return wrenches;
    }
}
